import 'package:flutter/material.dart';

class Melamar {
  final String nama;
  final String pesan;
  final String kontak;
  final String cv;

  const Melamar({
    required this.nama,
    required this.pesan,
    required this.kontak,
    required this.cv,
  });
}
