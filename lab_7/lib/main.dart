import 'package:flutter/material.dart';

void main() {
  runApp(MaterialApp(
    theme: ThemeData(
      brightness: Brightness.light,
      primaryColor: Colors.lightBlue[800],
      fontFamily: 'Arial',
    ),
    home: MyApp(),
  ));
}

class MyApp extends StatefulWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  _State createState() => _State();
}

class _State extends State<MyApp> {
  TextEditingController namaController = TextEditingController();
  TextEditingController pesanController = TextEditingController();
  TextEditingController kontakController = TextEditingController();
  TextEditingController cvController = TextEditingController();
  final _formKey = GlobalKey<FormState>();
  var edited = 0.0;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: const Text(
            'Home   FAQ   Buat Proyek   Proyek Saya ',
            style: TextStyle(
              color: Colors.grey,
            ),
          ),
          backgroundColor: Colors.white,
        ),
        body: Form(
            key: _formKey,
            child: Container(
                padding: const EdgeInsets.all(30),
                child: ListView(
                  children: <Widget>[
                    Container(
                        alignment: Alignment.center,
                        padding: const EdgeInsets.fromLTRB(20, 10, 20, 10),
                        child: const Text(
                          'Silahkan isi Form Berikut',
                          style: TextStyle(
                              color: Colors.black,
                              fontWeight: FontWeight.w500,
                              fontSize: 22),
                        )),
                    Container(
                      padding: const EdgeInsets.only(left: 10),
                      child: const Text(' Nama Pelamar',
                          textAlign: TextAlign.center),
                    ),
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: TextFormField(
                        decoration: new InputDecoration(
                          hintText: "Isi dengan nama sesuai identitas",
                          border: OutlineInputBorder(
                              borderRadius: new BorderRadius.circular(5.0)),
                        ),
                        validator: (value) {
                          if (value != null) {
                            if (value.isEmpty) {
                              return 'Nama tidak boleh kosong';
                            }
                            return null;
                          }
                        },
                      ),
                    ),
                    Container(
                      padding: const EdgeInsets.only(left: 10),
                      child: const Text(' Pesan Pelamar',
                          textAlign: TextAlign.center),
                    ),
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: TextFormField(
                        decoration: new InputDecoration(
                          hintText:
                              "Isi pesan yang ingin disampaikan kepada pembuat proyek",
                          border: OutlineInputBorder(
                              borderRadius: new BorderRadius.circular(5.0)),
                        ),
                        validator: (value) {
                          if (value != null) {
                            if (value.isEmpty) {
                              return 'Isi dengan - apabila tidak ingin menyampaikan pesan';
                            }
                            return null;
                          }
                        },
                      ),
                    ),
                    Container(
                      padding: const EdgeInsets.only(left: 10.0),
                      child: const Text(' Kontak Pelamar',
                          textAlign: TextAlign.center),
                    ),
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: TextFormField(
                        decoration: new InputDecoration(
                          hintText: "Isi dengan nama sesuai identitas",
                          border: OutlineInputBorder(
                              borderRadius: new BorderRadius.circular(5.0)),
                        ),
                        validator: (value) {
                          if (value != null) {
                            if (value.isEmpty) {
                              return 'Kontak tidak boleh kosong';
                            }
                            return null;
                          }
                        },
                      ),
                    ),
                    Container(
                      padding: const EdgeInsets.only(left: 10),
                      child: const Text(' CV Pelamar',
                          textAlign: TextAlign.center),
                    ),
                    Container(
                        height: 65,
                        padding: const EdgeInsets.fromLTRB(90, 10, 85, 10),
                        child: ElevatedButton(
                            child: const Text('Choose File'),
                            onPressed: () {
                              //butuh mempelajari file picker
                            },
                            style: ElevatedButton.styleFrom(
                                primary: Colors.lightGreen,
                                textStyle: const TextStyle(
                                  color: Colors.black12,
                                )))),
                    Row(
                      children: <Widget>[
                        const Text('*Masukkan File dengan Format ".pdf"'),
                      ],
                      mainAxisAlignment: MainAxisAlignment.center,
                    ),
                    Container(
                      height: 65,
                      padding: const EdgeInsets.fromLTRB(20, 10, 20, 10),
                      margin: EdgeInsets.all(8.0),
                      child: RaisedButton(
                        padding: EdgeInsets.all(8.0),
                        child: Text(
                          "Submit",
                          style: TextStyle(color: Colors.white),
                        ),
                        color: Colors.blue,
                        onPressed: () {
                          if (_formKey.currentState!.validate()) {
                            ScaffoldMessenger.of(context).showSnackBar(
                              const SnackBar(content: Text('Mengirim Data')),
                            );
                            setState(() {
                              edited = 1.0;
                            });
                          }
                        },
                      ),
                    ),
                  ],
                ))));
  }
}
