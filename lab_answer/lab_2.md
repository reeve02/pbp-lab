# Jawaban Pertanyaan Lab 2

## Perbedaan JSON dan XML

|XML          |JSON         |
| ----------- | ----------- |
| Merupakan bahasa markup yang biasanya digunakan untuk mengangkut data dari satu aplikasi ke aplikasi lain melalui internet | Merupakan format pertukaran data ringan yang jauh lebih mudah bagi komputer untuk menguraikan data yang sedang dikirim |
| Berorientasi dokumen | Berorientasi data |
| Transfer data lebih lambat karena penguraian lebih besar dan lambat | Transfer data lebih cepat karena ukuran file sangat kecil |
| Mendukung berbagai format encoding | Hanya mendukung encoding dengan UTF-8 |
| Tidak mendukung array | Mendukung penggunaan array |
| File size cenderung lebih besar | File size lebih kecil |
| Mendukung banyak tipe data, termasuk tipe data non primitif  | Tidak mendukung banyak tipe data |
| Menggunakan struktur data tree | Menggunakan struktur data pasangan key : value |
| Relatif lebih aman | Relatif kurang aman dibandingkan XML |
| Data pada XML tidak memiliki tipe | Objek pada JSON memiliki tipe |
| Support namespaces dan comment | Tidak support namespaces dan comment |
| Dapat digunakan untuk melakukan display | Tidak dapat melakukan display |

## Perbedaan HTML dan XML
|XML          |HTML         |
| ----------- | ----------- |
| Merupakan bahasa markup yang biasanya digunakan untuk mengangkut data dari satu aplikasi ke aplikasi lain melalui internet | Merupakan bahasa markup yang digunakan untuk membuat dan menampilkan halaman web |
| Banyak digunakan untuk keperluan transfer data | Banyak digunakan untuk menampilkan dan penyajian data |
| Bahasa XML bersifat case sensitive | HTML tidak case sensitive |
| Tag pada XML masih dapat dikembangkan dan didefinisikan sesuai kebutuhan programmer | Tag pada HTML sudah memiliki ketentuan dan cenderung tidak berubah |
| Support namespaces | Tidak support namespaces |

## Referensi
https://www.monitorteknologi.com/perbedaan-json-dan-xml/ 
https://www.guru99.com/json-vs-xml-difference.html 